function rot13(str) {
  const input = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const output = 'NOPQRSTUVWXYZABCDEFGHIJKLM';
  return str.split('').map(ch => {
    const i = input.indexOf(ch);
    if (i > -1) {
      return output[i];
    }
    return ch;
  }).join('');
}

rot13('SERR PBQR PNZC');
