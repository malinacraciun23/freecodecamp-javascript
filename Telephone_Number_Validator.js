function telephoneCheck(str) {
  let newStr = str;
  // stergem primul caracter daca e 1
  if (newStr[0] === '1') {
    newStr = newStr.substring(1).trim();
  }

  // verificam pentru (
  let i;
  let start = 0;
  while ((i = newStr.indexOf('(', start)) > -1) {
        if (newStr[i + 4] !== ')') return false;
        start = i + 1;
  }
  // verificam pentru )
  start = 0;
  while ((i = newStr.indexOf(')', start)) > -1) {
        if (newStr[i - 4] !== '(') return false;
        start = i + 1;
  }

  // verificam numarul separat sa fie format din str cu length >=3
  if (newStr.split(/-|\s/g).some(x => x.length < 3)) {
    return false;
  }

  // stergem caracterele speciale si spatiile si verificam sa fie 10 cifre
  newStr = newStr.replace(/-|\(|\)|\s/g, '');
  if (newStr.length !== 10) {
    return false;
  }
  return newStr.split('').every(c => !isNaN(parseInt(c, 10)));
}

telephoneCheck('1 (555) 555-5555');
