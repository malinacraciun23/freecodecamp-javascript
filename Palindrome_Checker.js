function palindrome(str) {
  const strText = str.replace(/[^a-z0-9+]+/gi, '');
  const strStripped = strText.replace(/\s+/g, '');
  const strLowered = strStripped.toLowerCase();
  const strArr = Array.from(strLowered);
  const reversedStrArr = [...strArr].reverse();

  return strArr.join('') === reversedStrArr.join('');
}


palindrome('eye');
