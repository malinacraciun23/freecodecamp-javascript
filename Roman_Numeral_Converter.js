function convertToRoman(n) {
  const result = [];
  const dec = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
  const roman = ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I'];
  for (let i = 0; i < dec.length; i++) {
    while (n >= dec[i]) {
        result.push(roman[i]);
        n -= dec[i];
      }
  }
  return result.join('');
}

convertToRoman(36);
